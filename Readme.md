#Description
Hubot connected to slack, and monitoring different url.
If an url is unreachable he will send a message in the channel of your choice, when url come back online he will send a new message in the channel.

#Configuration
* HUBOT_SLACK_TOKEN=xxx-xxx-xxx
* HUBOT_NOTIFICATIONS_ROOM=#builds
* HUBOT_HEALTHCHEKS=[{"id":"my-server-id-1", "url":"http://server-1.com", "watchInterval": 1000},{"id":"my-server-id-2", "url":"http://server-2.com", "watchInterval": 1000}]

#Usage
You can ask bot for different actions

* @bot status
* @bot add { "id": "prod", "url": "http://server-1.com", "watchInterval": 5000 }
* @bot remove <id>
