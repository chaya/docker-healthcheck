module.exports = function(robot) {

    robot.respond(/status/i, function(res) {
        robot.logger.info('Displaying status');
        for(var id in healthchecks) {
            var status = healthchecks[id].status ? ':large_blue_circle:' : ':red_circle:';
            res.send(healthchecks[id].id + '(' + healthchecks[id].url + '): ' + status + ' -> ' + healthchecks[id].msg);
        }
    });

    robot.respond(/add (.*)/i, function(respond) {
        robot.logger.info('Adding healthcheck ' + respond.match[1]);
        try {
            var healthcheck = JSON.parse(respond.match[1]);
            if(healthcheck && healthcheck.id && healthcheck.url && healthcheck.watchInterval) {
                robot.emit('healthcheck:add', healthcheck, respond);
            } else {
                respond.send('Wrong format, you need to write a javascript object with the following attributes: id, url, watchInterval. by example: {"id":"my-server-id", "url":"http://server.com", "watchInterval": 1000}');
            }
        } catch(err) {
            respond.send('Wrong format, you need to write a javascript object with the following attributes: id, url, watchInterval. by example: {"id":"my-server-id", "url":"http://server.com", "watchInterval": 1000}');
        }
    });

    robot.respond(/remove (.*)/i, function(respond) {
        robot.logger.info('Removing healthcheck ' + respond.match[1]);
        robot.emit('healthcheck:remove', respond.match[1], respond);
    });

    robot.on('healthcheck:add', function(healthcheck, respond) {
        //If we override an healthcheck, remove the old healthcheck before.
        robot.emit('healthcheck:remove', healthcheck);

        robot.logger.info('Adding healthcheck ' + healthcheck.id + '(' + healthcheck.url + ') every '+ healthcheck.watchInterval + 'ms');
        healthchecks[healthcheck.id] = healthcheck;
        (function(id) {
            healthchecks[id].intervalId = setInterval(function(){
                robot.emit('healthcheck:check', id);
            }, healthchecks[healthcheck.id].watchInterval);
        })(healthcheck.id);
        respond && respond.send(healthcheck.id + ' successfully added');
    });

    robot.on('healthcheck:remove', function(healthcheck, respond) {
        var id = healthcheck instanceof Object ? healthcheck.id : healthcheck;

        if(id && healthchecks[id]) {
            robot.logger.info('Removing healthcheck [' + healthcheck.url + ']');
            clearInterval(healthchecks[id].intervalId);
            delete healthchecks[healthcheck.id];
            respond && respond.send(healthcheck.id + ' successfully removed');
        } else {
            respond && respond.send(healthcheck.id + ' not found');
        }
    });

    robot.on('healthcheck:check', function(id) {
        robot.logger.info('Doing healthcheck ' + id + '(' + healthchecks[id].url+')');
        robot.http(healthchecks[id].url).get()(function(err, res, body) {
            robot.logger.info(healthchecks[id].url + ' is ' + res.statusCode);
            if(err || !/^2[0-9].*$/.test(res.statusCode)) {
                robot.logger.error(healthchecks[id].url + ' respond ' + res.statusCode);
                robot.logger.error(res.headers);

                if(healthchecks[id].status) {
                    robot.send({room: process.env.HUBOT_NOTIFICATIONS_ROOM}, ':red_circle:' + healthchecks[id].url + ' go down: ' + (err || res.statusCode));
                }
                healthchecks[id].status = false;
                healthchecks[id].msg = res.statusCode + ' ' + err;
            } else {
                healthchecks[id].msg = res.statusCode;
                if(!healthchecks[id].status) {
                    robot.send({room: process.env.HUBOT_NOTIFICATIONS_ROOM}, ':large_blue_circle:' + healthchecks[id].url + ' is back online: ' + res.statusCode);
                }
                healthchecks[id].status = true;
                healthchecks[id].msg = res.statusCode;
            }
        });
    });

    var healthchecks = {},
        healtcheckArray = eval(process.env.HUBOT_HEALTHCHEKS || []);

    robot.logger.info('Starting healthcheck');
    for(var i = 0; i < healtcheckArray.length; i++) {
        healthchecks[healtcheckArray[i].id] = healtcheckArray[i];
        robot.emit('healthcheck:add', healthchecks[healtcheckArray[i].id]);
    }
};